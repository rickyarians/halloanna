import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { Provider } from 'react-redux'
import store from '../Hambalang/src/redux/_store/store'

// Navigation
import Order from './src/view/navigations/order'

// Screen
import Splashscreen from './src/view/screens/splashScreen'


const MainNav = createAppContainer(createSwitchNavigator({
    // Splash Screen
    Main: {
      screen: Splashscreen
    },
     // Navigation Guest
    Order: {
      screen: Order
    },
  })
)



const App = () => {
  return (
    <Provider store={store}>
      <MainNav />
    </Provider>
  )
}
export default App