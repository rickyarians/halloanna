# Halloanna - Automation Restaurant

## Important

This project is still on very early development stage. If you want to use for production, use it with your own risk. More feature coming soon.

## Haloanna

Halooanna is an application that is useful for customer to choose menu and see the bill

## Screenshots
<p float="left">
 <img src="https://i.imgur.com/WHimfUw.jpg" width="200" height="350" alt="Home"/>
 <img src="https://i.imgur.com/TrNVBvn.jpg" width="200" height="350" alt="Register"/>
 <img src="https://i.imgur.com/3Sm5o9c.jpg" width="200" height="350" alt="Login"/>
 <img src="https://i.imgur.com/Azz2eFj.jpg" width="200" height="350" alt="Add Ads"/>
 <img src="https://i.imgur.com/KfxYF8o.jpg" width="200" height="350" alt="List"/>
 <img src="https://i.imgur.com/yJLOOZy.jpg" width="200" height="350" alt="Detail"/>
</p>


## Features

- Insert table number
- Choose Menu
- Increment Decrement Quantity
- View Bill

## Why Ayam Kamp Asix!?

- a simple application with powerful features
- It's React Native, so it support android & iOS by default
- Simple code, so you can easly contribute on it
- Express is one of the most starred javascript framework, and it is very easy to use

## Tech Stack

- React Native for the Mobile Frontend
- Express.js ad the backend

## Prerequisites

- Make sure You had been install NodeJs in your system [https://nodejs.org/](https://nodejs.org/)
- Then install React Native [https://facebook.github.io/react-native/](https://facebook.github.io/react-native/)
- Dont forget express ad the backend [https://expressjs.com/](https://expressjs.com/)

## Installation & Configuration

follow these steps to install

### Frontend

```bash
git clone https://github.com/Rickyarians/AyamKampAsix-RN.git
cd directory
npm install
npm start
react-native run-ios #for ios
react-native run-android #for android
```

create .env file and set API_HOST as your expressUri

```env
API_URL=http://YOUR_DOMAIN:YOUR_PORT
```

### Backend

```bash
git clone 
cd directory
npm install
npm install nodemon -D
npm start
```

## Support Us :)

- Stars this repository
- Hire Me

  * [Ricky Ariansyah](https://www.linkedin.com/in/rickyarians/)

## Contact

- Ricky Ariansyah
  * Wa/Telegram:  +62 823 10569056
  * E-Mail: rickyarians@outlook.com

 
## Download App (Demo usage only)
[![download](https://camo.githubusercontent.com/a9c59dcbf62ec123e8bb099fb473ad30554d70e6/68747470733a2f2f69312e77702e636f6d2f61706b6d6f6473696f732e636f6d2f77702d636f6e74656e742f75706c6f6164732f323031382f31322f446f776e6c6f61642d496e66696e6974652d44657369676e2d332e342e31302d41706b2e706e67 "Download")](https://drive.google.com/file/d/1Sd_EVHEGiUCHgiDuQOZHmyZHijM1_q9C/view?usp=sharing)
