import React from 'react'
import { Text, View, StyleSheet, StatusBar, ActivityIndicator, Dimensions, FlatList, TouchableOpacity, Image, YellowBox } from 'react-native'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
const { mainload } = require('./../../../redux/_actions/setTable')
const { getDataCemilan, getDataPromo, UpdatePromo } = require('./../../../redux/_actions/menu')

const CardProduk = require('./../../components/CardProduk')
import { API_URL } from 'react-native-dotenv'
import Iconicon from 'react-native-vector-icons/MaterialCommunityIcons'

class Promo extends React.Component {

    

    componentDidMount() {
        this.props.dispatch(mainload())
        this.props.dispatch(getDataPromo())
    }

    _tambahMakanan = async (item, datamakanann) => {
        let data = this.props.cart.Cart.findIndex(x => x.id == item.id)
        if (data >= 0) {
            // let data = this.state.datamkn.findIndex(y => y.id == item.id)
            // this.state.datamkn[data] = { ...this.state.datamkn[data], selected: true }
            // this.props.dispatch(tambahPesanan(item, this.props.setTable.idOrder))
            console.log(data)
        } else {
            // let data = this.state.datamkn.findIndex(y => y.id == item.id)
            // this.state.datamkn[data] = { ...this.state.datamkn[data], selected: true }
            this.props.dispatch(UpdatePromo(item, this.props.menu.dataPromo, this.props.menu.dataPromo))
            this.props.dispatch(tambahPesanan(item, this.props.setTable.idOrder))
        }
    }


    renderItem = ({ item, index }) => {

        return (
            <View key={item.id} style={styles.cardContainer}>

                <View style={styles.cardListBooking}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Image
                                source={{ uri: `${API_URL.replace('api/v1/', '')}${item.image}` }}
                                style={styles.imageIcon} />
                        </View>
                        <View style={{ flex: 2, padding: 5 }}>
                            <Text style={{ color: '#2ecc71', fontWeight: 'bold', fontSize: 14 }}>{item.name}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                <View style={[styles.detailBook, { flex: 5 }]}>
                                    <Text style={styles.textBook}>
                                        {this.toRupiah(item.price)}
                                    </Text>
                                </View>
                                <View style={[styles.detailBook, { flex: 2 }]}>

                                    {item.selected == false &&
                                        <TouchableOpacity onPress={() => this._tambahMakanan(item, this.props.setTable.idOrder)}>
                                            <View style={{ width: '100%', elevation: 1, backgroundColor: '#fff', borderColor: '#2ecc71', borderRadius: 5, padding: 2, borderWidth: 0.5 }}>
                                                <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 10 }}>Tambah</Text>
                                            </View>
                                        </TouchableOpacity>
                                    }

                                    {item.selected == true &&
                                        <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 10 }}>ditambahkan</Text>

                                    }

                                </View>
                            </View>

                        </View>
                    </View>
                </View>

            </View>
        )
    }

    render() {
        console.disableYellowBox = true
        return (
            <View style={styles.containerHome}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                {this.props.menu.isLoading == true &&
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="small" color="#2ecc71" />
                        <Text style={{ textAlign: 'center', fontSize: 12, color: '#2ecc71' }}>Sabar yaa.. Menunya Sedang Di Antar Ke layarmu</Text>
                    </View>
                }

                {this.props.menu.isLoading == false && this.props.menu.dataPromo.length == 0 &&

                    <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5, paddingBottom: 0, paddingTop: 0, position: 'relative' }}>
                        <View style={{ flex: 1, justifyContent: 'center', padding: 40, borderRadius: 20 }}>
                            <Image
                                style={{ width: '100%', height: '100%', resizeMode: 'cover', marginTop: 20 }}
                                source={require('./../../../assets/images/tidakada.png')}
                            />
                            <Text style={{ color: '#2ecc71', fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>Waduh... Belum ada Promo Nih Gaes..</Text>
                        </View>
                        <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', alignSelf: 'center' }}>

                            <View style={{ flex: 1, flexDirection: 'row', position: 'absolute', height: 50, backgroundColor: '#2ecc71', width: '100%', bottom: 10, elevation: 1, borderRadius: 5, padding: 5 }}>

                                <View style={{ flex: 4 }}>
                                    <Iconicon name="silverware" size={16} color={'#fff'} />
                                    <Text style={{ size: 16, color: '#fff' }}>{this.props.cart.Cart.length + ' Menu Telah Ditambahkan'}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailOrder')}>
                                        <View style={{ backgroundColor: '#2ecc71', borderRadius: 5, borderColor: '#fff', borderWidth: 0.8, paddingVertical: 10, paddingHorizontal: 1 }}>
                                            <Text style={{ color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>Detail</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </View>

                        </View>
                    </View>

                }
                {this.props.menu.isLoading == false && this.props.menu.dataPromo.length > 0 &&
                    <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5, paddingBottom: 0, paddingTop: 0, position: 'relative' }}>
                        <FlatList
                            data={this.props.menu.dataPromo}
                            showsVerticalScrollIndicator={false}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => item.id}
                            extraData={this.props.menu.dataPromo}
                        />
                        <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', alignSelf: 'center' }}>


                            <View style={{ flex: 1, flexDirection: 'row', position: 'absolute', height: 50, backgroundColor: '#2ecc71', width: '100%', bottom: 10, elevation: 1, borderRadius: 5, padding: 5 }}>

                                <View style={{ flex: 4 }}>
                                    <Iconicon name="silverware" size={16} color={'#fff'} />
                                    <Text style={{ size: 16, color: '#fff' }}>{this.props.cart.Cart.length + ' Menu Telah Ditambahkan'}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailOrder')}>
                                        <View style={{ backgroundColor: '#2ecc71', borderRadius: 5, borderColor: '#fff', borderWidth: 0.8, paddingVertical: 10, paddingHorizontal: 1 }}>
                                            <Text style={{ color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>Detail</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </View>


                        </View>
                    </View>
                }

            </View >

        )
    }
}


const mapStateToProps = (state) => {
    return {
        menu: state.menu,
        setTable: state.setTable,
        cart: state.cart
    }
}

export default connect(mapStateToProps)(Promo);

const styles = StyleSheet.create({
    containerHome: {
        flex: 1,
        backgroundColor: '#fff',
    },
    cardStatus: {
        marginTop: 6,
        borderWidth: 1,
        borderColor: '#03A9F4',
        width: '100%',
        padding: 2,
        borderRadius: 5,
    },
    cardListBooking: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#eeeeee',
        height: 100,
        elevation: 1
    },
    textBook: {
        fontSize: 16,
        color: '#000',
    },
    detailBook: {
        flex: 1,
        marginRight: 15,
    },
    searchBar: {
        paddingTop: 10,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#03A9F4',
        height: 60,
        marginBottom: 0,
        flexDirection: 'row'
    },
    seactInput: {
        paddingHorizontal: 40,
        padding: 5,
        height: 40,
        borderRadius: 5,
        borderColor: '#03A9F4',
        backgroundColor: '#CFD8DC',
        borderWidth: 1
    },
    imageIcon: {
        width: 100,
        height: '100%',
        borderRadius: 2,
    },
    touchable: {
        position: 'absolute',
        top: 3,
        left: 10,
    },
    cardContainer: {
        marginBottom: 10,
        marginTop: 2,
    },
    starIconContainer: {
        position: 'absolute',
        top: 5,
        right: 10,
    },
    textDefault: {
        fontSize: 10,
        color: '#BDBDBD'
    },
    textSeparator: {
        marginHorizontal: 5,
        marginBottom: 10
    },
    kostName: {
        color: '#757575',
        fontSize: 10,
        flex: 1
    },
    textPrice: {
        fontWeight: '600',
        marginTop: -5,
        fontSize: 12
    },
    textUpdated: {
        color: '#757575',
        fontSize: 10,
        flex: 1,
        marginLeft: 5,
        paddingBottom: 5
    }
})