import React from "react";
import { TextInput, StatusBar, View, Text, Dimensions, ActivityIndicator, StyleSheet, Image, Button, TouchableOpacity } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5"
import AsyncStorage from '@react-native-community/async-storage'
import styles from './../styles/splashStyle'
const { height, width } = Dimensions.get('window')



class SplashScreen extends React.Component {
  constructor() {
    super();
  }


  _splash = () => {
    this.props.navigation.navigate('Order')
  }

  componentDidMount() {
    setTimeout(this._splash, 3000)
  }



  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />


        <View style={styles.containerImage}>
          <Image
            style={styles.image}
            source={require('./../../assets/images/lunch.png')}
          />
        </View>

        <View style={{ marginVertical: 40, paddingHorizontal: 15, width: width }}>
          <View style={{flex:1}}>
            <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#2ecc71', textAlign: 'center' }}>
              Ayam Kampus Asix!
          </Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', marginTop: 50 }}>
            <ActivityIndicator size="small" color="#2ecc71" />

          </View>
        </View>


      </View>
    )
  }
}

export default SplashScreen

