import React from "react";
import { TextInput, StatusBar, View, Text, Dimensions, StyleSheet, Image, Button, TouchableOpacity, ActivityIndicator } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5"
import AsyncStorage from '@react-native-community/async-storage'
import styles from './../../styles/setTable'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
const { handlingtable, postTable, SetTime, SetTimemulai } = require('./../../../redux/_actions/setTable')



class setTable extends React.Component {
  constructor() {
    super();
    this.state = {
      Loading: false
    }
  }


  async componentDidUpdate() {
    if (this.props.setTable.berhasilsetTable == true) {
      await this.props.dispatch(SetTimemulai())
      await this.props.navigation.navigate('Menu')
    }
  }


  async componentDidMount () {
    await this.props.dispatch(SetTime())
  
  }
  _setFalse = () => {
    this.setState({
      Loading: false
    })
  }

  _setLoading = () => {
    this.setState({
      Loading: true
    })
  setInterval(this._setFalse, 2000)
   this.props.dispatch(postTable(this.props.setTable.table_number))

  }



  render() {
  
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
        <View style={styles.containerImage}>
          <Image
            style={styles.image}
            source={require('./../../../assets/images/lunch.png')}
          />
        </View>

        <View style={{ marginVertical: 10, paddingHorizontal: 15, width: width }}>
          <Text style={{ color: '#2ecc71', fontWeight: 'bold' }}>Masukkan Nomor Meja {this.props.setTable.table_number}</Text>
          <TextInput value={this.props.setTable.table_number} style={{ height: 40, width: '100%', borderBottomColor: '#2ecc71', borderBottomWidth: 1, textAlignVertical: 'bottom', color: '#2ecc71' }} keyboardType={'numeric'} onChangeText={(number_table) => this.props.dispatch(handlingtable(number_table))} />

          {this.state.Loading == false &&
            <TouchableOpacity onPress={() => this._setLoading()}>
              <View style={{ backgroundColor: '#fff', borderRadius: 10, padding: 10, marginTop: 10, width: '100%', borderColor: '#2ecc71', borderWidth: 1.5, elevation: 1 }}>
                <Text style={{ textAlign: 'center', color: '#2ecc71', fontWeight: 'bold' }}>Submit</Text>
              </View>
            </TouchableOpacity>
          }


          {this.state.Loading == true &&
            <View style={{ backgroundColor: '#fff', borderRadius: 10, padding: 10, marginTop: 10, width: '100%', borderColor: '#2ecc71', borderWidth: 1.5, elevation: 1 }}>
              <ActivityIndicator size="small" color="#2ecc71" />
            </View>
          }
        </View>


      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    setTable: state.setTable
  }
}

export default connect(mapStateToProps)(setTable);
