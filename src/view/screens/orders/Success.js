import React from "react";
import { TextInput, StatusBar, View, Text, Dimensions, StyleSheet, Image, Button, TouchableOpacity, ActivityIndicator } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5"
import AsyncStorage from '@react-native-community/async-storage'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
const { handlingtable, postTable } = require('./../../../redux/_actions/setTable')
import Barcode from 'react-native-barcode-builder'


class Success extends React.Component {
  constructor() {
    super();
    this.state = {
      Loading: false
    }
  }


  componentDidUpdate() {
    if (this.props.setTable.berhasilsetTable == true) {
      this.props.navigation.navigate('Menu')
    }
  }

  _setFalse = () => {
    this.setState({
      Loading: false
    })
  }

  _setLoading = () => {
    this.setState({
      Loading: true
    })
  setInterval(this._setFalse, 2000)
   this.props.dispatch(postTable(this.props.setTable.table_number))

  }



  render() {
  
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
        <View style={styles.containerImage}>
          <Image
            style={styles.image}
            source={require('./../../../assets/images/success-goal.png')}
          />
        </View>

        <View style={{ marginVertical: 10, paddingHorizontal: 15, width: width }}>
          <Text style={{ color: '#2ecc71', fontWeight: 'bold', textAlign: 'center'}}>Transaksi Berhasil</Text>
          <Text style={{ color: '#2ecc71', fontWeight: 'bold', textAlign: 'center'}}>{'#' + this.props.setTable.idOrder}</Text>
          <Text style={{ color: '#2ecc71', fontWeight: 'bold', textAlign: 'center'}}>{'Table : ' + this.props.setTable.table_number}</Text>
          <Barcode value={this.props.setTable.idOrder.toString()} height='50' lineColor='#2ecc71' format='CODE128'/>
        </View>


      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    setTable: state.setTable
  }
}

export default connect(mapStateToProps)(Success);


const styles = StyleSheet.create({
  container: {
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 1,
      height: height
  },
  containerImage: {
      height: 2/4 * height,
      
  },
  textTagline: {
      color: '#2ecc71',
      fontWeight: 'bold',
      fontSize: 24
  },
  image: {
      resizeMode: 'cover',
      height: 2/4 * height,
      width: width
  }
})
