import React from 'react'
import { Text, View, StyleSheet, StatusBar, ActivityIndicator, Dimensions, FlatList, TouchableOpacity, Image } from 'react-native'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
const { getDataMakanan, getDataMinuman } = require('./../../../redux/_actions/menu')
const { Increment, Decrement } = require('./../../../redux/_actions/cart')
const { changesuccess } = require('./../../../redux/_actions/setTable')
const { tambahPesanan } = require('./../../../redux/_actions/cart')
const { getTransaction, updateOrder, changeStatusOrder, changeStatusTrx } = require('./../../../redux/_actions/transaction')
const CardProduk = require('./../../components/CardProduk')
import { API_URL } from 'react-native-dotenv';
import Iconicon from 'react-native-vector-icons/Feather'
import Success from './Success';


class Bill extends React.Component {
    constructor() {
        super()
        this.state = {
            subtotal: '',
            service: '',
            pajak: '',
            total: '',
            discount: '',
            buttoncall: false
        }
    }







    _hitungtotal = async () => {
        let datatotal = 0
        await this.props.cart.CartFix.map(item => {
            let totalhargabarang = item.qty * item.price
            console.log(totalhargabarang)
            datatotal = datatotal + totalhargabarang
        })
        console.log(datatotal)
        this.setState({
            totalharga: datatotal,
        })

    }

    _checkorder = async () => {
        await this.props.transaction.order.map(item => {
            this.props.dispatch(updateOrder(item, this.props.transaction.order, this.props.transaction.order))
        })

        await setTimeout(() => {
            this.setState({
                buttoncall: true
            })
        }, 2000);
    }

    async componentDidMount() {
        await this.props.dispatch(getTransaction(this.props.setTable.idOrder))
        await this.setState({
            subtotal: this.props.transaction.transaction.subtotal,
            service: this.props.transaction.transaction.service,
            pajak: this.props.transaction.transaction.tax,
            total: this.props.transaction.transaction.total,
            discount: this.props.transaction.transaction.discount
        })

        await setTimeout(() => {
            this._checkorder()
        }, 10000);
       

    }




    toRupiah = (number) => {
        let rupiah = '';
        let revNumber = number.toString().split('').reverse().join('');
        for (var i = 0; i < revNumber.length; i++) if (i % 3 == 0) rupiah += revNumber.substr(i, 3) + '.';
        return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
    }

    datetime = (time) => {
        let Menit = Math.floor(time / 60);
        let Detik = time % 60;
        return Menit.toString() + ":" + Detik.toString();
    
      }

    _success = async () => {
        await this.props.dispatch(changeStatusOrder(this.props.setTable.idOrder))
        await this.props.dispatch(changeStatusTrx(this.props.setTable.idOrder,this.datetime(this.props.timer.counter),this.props.transaction.transaction))
        await this.props.dispatch(changesuccess())
        await this.props.navigation.navigate('Success')
    }

    renderItem = ({ item, index }) => {

        return (
            <View key={item.id} style={styles.cardContainer}>

                <View style={styles.cardListBooking}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            {item.status == 0 &&
                                <Text style={{ color: 'yellow', fontSize: 12 }}>Waiting</Text>
                            }
                            {item.status == 1 &&
                                <Text style={{ color: 'green', fontSize: 12 }}>Sent</Text>
                            }
                        </View>
                        <View style={{ flex: 3 }}>
                            <Text style={{ fontSize: 12, color: '#2ecc71' }}>{item.namaa_menu.name}</Text>
                            <Text style={{ fontSize: 12, color: '#bdbdbd' }}>{item.qty + ' x ' + this.toRupiah(item.price)}</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 12, color: '#2ecc71' }}>{this.toRupiah(item.price * item.qty)}</Text>
                        </View>
                    </View>
                </View>

            </View>
        )
    }




    render() {
        console.log(this.props.setTable.idOrder)
        console.log(this.props.transaction.order)
        console.log(API_URL)
        return (

            <View style={styles.containerHome}>
                <StatusBar barStyle="light-content" backgroundColor="#fff" />
                <View style={{ flex: 1, }}>
                    <View style={{ flex: 1, paddingTop: 15, paddingRight: 5, paddingLeft: 5, paddingBottom: 5, paddingBottom: 0, paddingTop: 0, borderBottomRightRadius: 5, borderBottomLeftRadius: 5, borderBottomColor: '#bdbd', borderRightColor: '#bdbd', borderLeftColor: '#bdbd', marginBottom: 5, borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, }}>
                        <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 16, fontWeight: 'bold', textAlignVertical: 'center', marginTop: 15 }}>Bill Pesanan</Text>
                    </View>
                </View>

                <View style={{ flex: 10 }}>
                    <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5, paddingBottom: 0, paddingTop: 0 }}>
                        <FlatList
                            data={this.props.transaction.order}
                            showsVerticalScrollIndicator={false}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => item.id}
                            extraData={this.props.transaction.order}
                        />
                    </View>
                </View>

                <View style={{ flex: 2, paddingTop: 5, paddingRight: 10, paddingLeft: 10, backgroundColor: '#fff', borderTopWidth: 0.5, borderColor: '#bdbdbd', borderTopLeftRadius: 2, borderTopRightRadius: 2, flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Text style={{ textAlign: 'left', color: '#000', fontSize: 12 }}>Subtotal : </Text>
                        <Text style={{ textAlign: 'left', color: '#000', fontSize: 12 }}>Potongan Harga : </Text>
                        <Text style={{ textAlign: 'left', color: '#000', fontSize: 12 }}>Biaya Layanan (5%) : </Text>
                        <Text style={{ textAlign: 'left', color: '#000', fontSize: 12 }}>Pajak (10%) : </Text>
                        <Text style={{ textAlign: 'left', color: '#2ecc71', fontSize: 14 }}>Total : </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ textAlign: 'right', color: '#000', fontSize: 12 }}>{this.toRupiah(this.state.subtotal)}</Text>
                        <Text style={{ textAlign: 'right', color: '#000', fontSize: 12 }}>{this.toRupiah(this.state.discount)}</Text>
                        <Text style={{ textAlign: 'right', color: '#000', fontSize: 12 }}>{this.toRupiah(this.state.service)}</Text>
                        <Text style={{ textAlign: 'right', color: '#000', fontSize: 12 }}>{this.toRupiah(this.state.pajak)}</Text>
                        <Text style={{ textAlign: 'right', color: '#2ecc71', fontSize: 14 }}>{this.toRupiah(this.state.total)}</Text>
                    </View>
                </View>
                <View style={{ flex: 1, paddingBottom: 5 }}>
                    {this.state.buttoncall == true &&
                        <View style={{ flex: 1, backgroundColor: '#fff', borderTopLeftRadius: 2, borderTopRightRadius: 2, paddingBottom: 10 }}>
                            <TouchableOpacity onPress={() => this._success()}>
                                <View style={{ backgroundColor: '#2ecc71', padding: 10, marginTop: 5, marginRight: 5, marginLeft: 5, borderRadius: 5, borderColor: '#bdbdbd', borderWidth: 0.5 }}>
                                    <Text style={{ textAlign: 'center', color: '#fff' }}>Call Bill</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    }

                    { this.state.buttoncall == false &&
                        <View style={{ flex: 1, backgroundColor: '#fff', borderTopLeftRadius: 2, borderTopRightRadius: 2, paddingBottom: 10 }}>

                            <View style={{ backgroundColor: '#bdbdbd', padding: 10, marginTop: 5, marginRight: 5, marginLeft: 5, borderRadius: 5, borderColor: '#bdbdbd', borderWidth: 0.5 }}>
                                <Text style={{ textAlign: 'center', color: '#fff' }}>Call Bill</Text>
                            </View>


                        </View>
                    }


                </View>




            </View >

        )
    }
}


const mapStateToProps = (state) => {
    return {
        menu: state.menu,
        setTable: state.setTable,
        cart: state.cart,
        transaction: state.transaction,
        timer: state.timer
    }
}

export default connect(mapStateToProps)(Bill);

const styles = StyleSheet.create({
    containerHome: {
        flex: 1,
        backgroundColor: '#fff',
    },
    cardStatus: {
        marginTop: 6,
        borderWidth: 1,
        borderColor: '#03A9F4',
        width: '100%',
        padding: 2,
        borderRadius: 5,
    },
    cardListBooking: {
        borderBottomWidth: 0.5,
        borderColor: '#bdbdbd'
    },
    textBook: {
        fontSize: 14,
        color: '#bdbdbb',
    },
    detailBook: {
        flex: 1,
        marginRight: 15,
    },
    searchBar: {
        paddingTop: 10,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#03A9F4',
        height: 60,
        marginBottom: 0,
        flexDirection: 'row'
    },
    seactInput: {
        paddingHorizontal: 40,
        padding: 5,
        height: 40,
        borderRadius: 5,
        borderColor: '#03A9F4',
        backgroundColor: '#CFD8DC',
        borderWidth: 1
    },
    imageIcon: {
        width: 100,
        height: '100%',
        borderRadius: 2,
    },
    touchable: {
        position: 'absolute',
        top: 3,
        left: 10,
    },
    cardContainer: {
        marginBottom: 5,
        marginTop: 2,
    },
    starIconContainer: {
        position: 'absolute',
        top: 5,
        right: 10,
    },
    textDefault: {
        fontSize: 10,
        color: '#BDBDBD'
    },
    textSeparator: {
        marginHorizontal: 5,
        marginBottom: 10
    },
    kostName: {
        color: '#757575',
        fontSize: 10,
        flex: 1
    },
    textPrice: {
        fontWeight: '600',
        marginTop: -5,
        fontSize: 12
    },
    textUpdated: {
        color: '#757575',
        fontSize: 10,
        flex: 1,
        marginLeft: 5,
        paddingBottom: 5
    }
})