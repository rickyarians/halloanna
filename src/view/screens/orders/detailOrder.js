import React from 'react'
import { Text, View, StyleSheet, StatusBar, ActivityIndicator, Dimensions, FlatList, TouchableOpacity, Image, Alert } from 'react-native'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
const { getDataMakanan, getDataMinuman, FalseMakanan } = require('./../../../redux/_actions/menu')
const { Increment, Decrement } = require('./../../../redux/_actions/cart')
const { tambahPesanan, TambahOrders, hapusPesanan } = require('./../../../redux/_actions/cart')
const CardProduk = require('./../../components/CardProduk')
import { API_URL } from 'react-native-dotenv';
import Iconicon from 'react-native-vector-icons/Feather'



class detailOrder extends React.Component {
    constructor() {
        super()
        this.state = {
            totalharga: '',

        }
    }







    _hitungtotal = async () => {
        let datatotal = 0
        await this.props.cart.Cart.map(item => {
            let totalhargabarang = item.qty * item.price
            console.log(totalhargabarang)
            datatotal = datatotal + totalhargabarang
        })
        console.log(datatotal)
        this.setState({
            totalharga: datatotal
        })

    }

    componentDidMount() {
        this._hitungtotal()
    }

    _increment = (item) => {
        this.props.dispatch(Increment(item, this.props.cart.Cart, this.props.cart.Cart))
        this._hitungtotal()
    }


    _decrement = async (item) => {
       if (item.qty <= 0 || item.qty == 1) {
        await this.props.dispatch(hapusPesanan(item, this.props.cart.Cart, this.props.cart.Cart))
        await this.props.dispatch(FalseMakanan(item, this.props.menu.dataProduk, this.props.menu.dataProduk))
        await this._hitungtotal()
        // if(this.props.cart.Cart == null) {
        //     this.props.navigation.goBack();
        // }
       } else {
        this.props.dispatch(Decrement(item, this.props.cart.Cart, this.props.cart.Cart))
        this._hitungtotal()
       }
    }

    componentDidUpdate() {
        if (this.props.cart.berhasilbill == true) {
            this.props.navigation.navigate('BillOrder')
        }
    }

    alertConfirm = () => {
        Alert.alert(
            'Konfirmasi Pesanan',
            'Apakah anda yakin pesanan sudah sesuai',
            [
                {
                    text: 'Batalkan',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.props.dispatch(TambahOrders(this.props.cart.Cart)) },
            ],
            { cancelable: false },
        );
    }

    _toggleModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        })
    }




    toRupiah = (number) => {
        let rupiah = '';
        let revNumber = number.toString().split('').reverse().join('');
        for (var i = 0; i < revNumber.length; i++) if (i % 3 == 0) rupiah += revNumber.substr(i, 3) + '.';
        return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
    }

    renderItem = ({ item, index }) => {

        return (
            <View key={item.id} style={styles.cardContainer}>

                <View style={styles.cardListBooking}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Image
                                source={{ uri: `${API_URL.replace('api/v1/', '')}${item.image}` }}
                                style={styles.imageIcon} />
                        </View>
                        <View style={{ flex: 2, padding: 5 }}>
                            <Text style={{ color: '#2ecc71', fontWeight: 'bold', fontSize: 12 }}>{item.name}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 0.5 }}>
                                    <Text style={{ color: '#2ecc71', textAlign: 'left', fontSize: 12 }}>
                                        {item.qty + ' x '}
                                    </Text>
                                </View>
                                <View style={{ flex: 2 }}>
                                    <Text style={{ textAlign: 'left', fontSize: 12, color: '#bdbdbd' }}>
                                        {this.toRupiah(item.price)}
                                    </Text>
                                </View>

                                <View style={[styles.detailBook, { flex: 1 }]}>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <TouchableOpacity onPress={() => this._decrement(item)}>
                                                <View style={{ width: '100%', elevation: 1, backgroundColor: '#fff', borderColor: '#2ecc71', borderRadius: 5, padding: 2, borderWidth: 0.5 }}>
                                                    <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 10 }}>-</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ width: '100%', padding: 2, }}>
                                                <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 10 }}>{item.qty}</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <TouchableOpacity onPress={() => this._increment(item)}>
                                                <View style={{ width: '100%', elevation: 1, backgroundColor: '#fff', borderColor: '#2ecc71', borderRadius: 5, padding: 2, borderWidth: 0.5 }}>
                                                    <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 10 }}>+</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                            </View>
                            <View>
                                <Text style={{ fontSize: 12, color: '#2ecc71' }}>{this.toRupiah(item.qty * item.price)}</Text>
                            </View>

                        </View>
                    </View>
                </View>

            </View>
        )
    }




    render() {
        console.log(this.props.cart.Cart)
        return (

            <View style={styles.containerHome}>
                <StatusBar barStyle="light-content" backgroundColor="#fff" />
                <View style={{ flex: 1, }}>
                    <View style={{ flex: 1, paddingTop: 15, paddingRight: 5, paddingLeft: 5, paddingBottom: 5, paddingBottom: 0, paddingTop: 0, borderBottomRightRadius: 5, borderBottomLeftRadius: 5, borderBottomColor: '#bdbd', borderRightColor: '#bdbd', borderLeftColor: '#bdbd', marginBottom: 5, borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, }}>
                        <Text style={{ color: '#2ecc71', textAlign: 'center', fontSize: 16, fontWeight: 'bold', textAlignVertical: 'center', marginTop: 15 }}>Detail Pesanan</Text>
                    </View>
                </View>

                <View style={{ flex: 10 }}>
                    <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5, paddingBottom: 0, paddingTop: 0 }}>
                        <FlatList
                            data={this.props.cart.Cart}
                            showsVerticalScrollIndicator={false}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => item.id}
                        />
                    </View>
                </View>


                {this.props.cart.Cart.length != 0 &&

                    <View style={{ flex: 2, borderTopWidth: 1, borderColor: '#bdbdbd', }}>
                        <View style={{ flex: 0.5, padding: 10 }}>
                            <Text style={{ fontSize: 16, color: '#2ecc71' }}>
                                {'Subtotal : ' + this.toRupiah(this.state.totalharga)}
                            </Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>

                            <View style={{ flex: 1, backgroundColor: '#fff', height: 50, borderTopLeftRadius: 2, borderTopRightRadius: 2 }}>
                                <TouchableOpacity onPress={() => this.alertConfirm()} >
                                    <View style={{ backgroundColor: '#2ecc71', padding: 10, marginTop: 5, marginRight: 5, marginLeft: 5, marginBottom: 5, borderRadius: 5 }}>
                                        <Text style={{ textAlign: 'center', color: '#fff' }}>Konfirmasi</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                          
                        </View>
                    </View>


                }

                {
                    this.props.cart.Cart.length == 0 &&
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1, backgroundColor: '#fff', borderTopWidth: 0.5, borderColor: '#bdbdbd', height: 50, borderTopLeftRadius: 2, borderTopRightRadius: 2 }}>

                            <View style={{ backgroundColor: '#bdbdbd', padding: 10, marginTop: 5, marginRight: 5, marginLeft: 5, marginBottom: 5, borderRadius: 5, borderColor: '#bdbdbd', borderWidth: 0.5 }}>
                                <Text style={{ textAlign: 'center', color: '#fff' }}>Konfirmasi</Text>
                            </View>

                        </View>
                       
                    </View>
                }

            </View >

        )
    }
}


const mapStateToProps = (state) => {
    return {
        menu: state.menu,
        setTable: state.setTable,
        cart: state.cart,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(detailOrder);

const styles = StyleSheet.create({
    containerHome: {
        flex: 1,
        backgroundColor: '#fff',
    },
    cardStatus: {
        marginTop: 6,
        borderWidth: 1,
        borderColor: '#03A9F4',
        width: '100%',
        padding: 2,
        borderRadius: 5,
    },
    cardListBooking: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#eeeeee',
        height: 80,
        elevation: 1
    },
    textBook: {
        fontSize: 14,
        color: '#bdbdbb',
    },
    detailBook: {
        flex: 1,
        marginRight: 15,
    },
    searchBar: {
        paddingTop: 10,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#03A9F4',
        height: 60,
        marginBottom: 0,
        flexDirection: 'row'
    },
    seactInput: {
        paddingHorizontal: 40,
        padding: 5,
        height: 40,
        borderRadius: 5,
        borderColor: '#03A9F4',
        backgroundColor: '#CFD8DC',
        borderWidth: 1
    },
    imageIcon: {
        width: 100,
        height: '100%',
        borderRadius: 2,
    },
    touchable: {
        position: 'absolute',
        top: 3,
        left: 10,
    },
    cardContainer: {
        marginBottom: 5,
        marginTop: 2,
    },
    starIconContainer: {
        position: 'absolute',
        top: 5,
        right: 10,
    },
    textDefault: {
        fontSize: 10,
        color: '#BDBDBD'
    },
    textSeparator: {
        marginHorizontal: 5,
        marginBottom: 10
    },
    kostName: {
        color: '#757575',
        fontSize: 10,
        flex: 1
    },
    textPrice: {
        fontWeight: '600',
        marginTop: -5,
        fontSize: 12
    },
    textUpdated: {
        color: '#757575',
        fontSize: 10,
        flex: 1,
        marginLeft: 5,
        paddingBottom: 5
    }
})