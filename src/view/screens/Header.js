import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, Dimensions, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { API_URL } from 'react-native-dotenv'
import { setTimer, setTimerReset  } from './../../redux/_actions/timer'
import { changeunsuccess} from './../../redux/_actions/setTable'
import { resetbill} from './../../redux/_actions/cart'
const { height, width } = Dimensions.get('window')
import { connect } from 'react-redux';
import AppNavigator from './../navigations/menu';
const AppIndex = createAppContainer(AppNavigator)


class Header extends Component {
  constructor() {
    super()
    this.state = {
      timer: null,
      time: 0
    }
  }

  count = () => {
    this.setState({
      time: this.state.time + 1
    })
    this.props.dispatch(setTimer(this.state.time))
  }

  datetime = (time) => {
    let Menit = Math.floor(time / 60);
    let Detik = time % 60;
    return Menit + ":" + Detik;

  }

  _destroyEverything = async () => {
    // ubah success order false
    await this.props.dispatch(setTimerReset())
    await this.props.dispatch(changeunsuccess())
    await this.props.dispatch(resetbill())
    await this._handlerstop()
    this.props.navigation.navigate('setTable')
  }

  componentDidMount() {
    this.interval = setInterval(this.count, 1000);
  }


  _handlerstop = () => {
    clearInterval(this.interval)
  }

  render() {
    return (
      <View style={{ flex: 1 }} >
        <StatusBar
          backgroundColor='#fff'
          barStyle='dark-content'
        />
        {
          this.props.setTable.successorder == false &&
          <View style={styles.header}>

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <Icon name='book-open' size={16} color='#2ecc71' />
              <Text style={{ fontSize: 12, color: '#2ecc71', fontWeight: 'bold' }}>
                {' ' + this.props.setTable.table_number}
              </Text>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <Icon name='clock' size={16} color='#2ecc71' />
              <Text style={{ fontSize: 12, color: '#2ecc71', fontWeight: 'bold' }}>{' ' + this.datetime(this.props.timer.counter)}</Text>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', alignContent: 'center', alignItems: 'center', justifyContent: 'space-evenly' }}>
              <Icon name='trello' size={16} color='#2ecc71' style={{ textAlign: 'center' }} />
              <Text style={{ fontSize: 12, color: '#2ecc71', fontWeight: 'bold', textAlign: 'right' }}>
                {' ' + this.props.setTable.idOrder}
              </Text>
            </View>

          </View>
        }

        {
          this.props.setTable.successorder == true &&
          <View style={styles.header}>

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => this._destroyEverything()}>
                  <View>
                    <Text style={{color: '#2ecc71'}}>Kembali</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{ flex: 1 }}>

              </View>

              <View style={{ flex: 1 }}>
          
              </View>
            </View>

          </View>
        }
        <AppIndex />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    menu: state.menu,
    setTable: state.setTable,
    cart: state.cart,
    timer: state.timer
  }
}

export default connect(mapStateToProps)(Header);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingHorizontal: 18,
    paddingTop: 5,
  }
});  