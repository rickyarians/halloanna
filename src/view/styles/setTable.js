import {StyleSheet, Dimensions} from 'react-native'
const {height, width } = Dimensions.get('window')


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerImage: {
        height: 2/4 * height,
        
    },
    textTagline: {
        color: '#C40C41',
        fontWeight: 'bold',
        fontSize: 24
    },
    image: {
        resizeMode: 'cover',
        height: 2/4 * height,
        width: width
    }
  })


export default styles;