import { createStackNavigator } from 'react-navigation'


// import screen
import setTable from './../screens/orders/setTable'
import Menu from './../screens/Header'
import  Success from './../screens/orders/Success'


const OrderStack = createStackNavigator({
    setTable: {
        screen: setTable
    },
    Menu: {
        screen: Menu
    },
    Success: {
        screen: Success
    }


},
    {
        mode: 'modal',
        headerMode: 'none',
    })






export default OrderStack