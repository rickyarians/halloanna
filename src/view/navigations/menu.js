import React from 'react'
import { Text, View, Stylesheet } from 'react-native'
import { TabNavigator, MaterialTopTabBar, createStackNavigator, createMaterialTopTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation'
import Iconicon from 'react-native-vector-icons/MaterialCommunityIcons'


//screen 
import Promo from './../screens/menu/Promo'
import Makanan from './../screens/menu/Makanan'
import Minuman from './../screens/menu/Minuman'
import Cemilan from './../screens/menu/Cemilan'
import DetailOrder from './../screens/orders/detailOrder'
import BillOrder from './../screens/orders/Bill'
import Success from '../screens/orders/Success';

// screen 
const OrderTab = createMaterialTopTabNavigator({
  promo: {
    screen: Promo,
    navigationOptions: {
      tabBarLabel: "Promo",
      tabBarIcon: ({ tintColor }) => (
        <Iconicon name="food-variant" size={20} color={tintColor} />
      )
    }
  },
  makanan: {
    screen: Makanan,
    navigationOptions: {
      tabBarLabel: "Makanan",
      tabBarIcon: ({ tintColor }) => (
        <Iconicon name="food" size={20} color={tintColor} />
      )
    }
  },
  minuman: {
    screen: Minuman,
    navigationOptions: {
      tabBarLabel: "Minuman",
      tabBarIcon: ({ tintColor }) => (
        <Iconicon name="food-fork-drink" size={20} color={tintColor} />
      )
    }
  },
  cemilan: {
    screen: Cemilan,
    navigationOptions: {
      tabBarLabel: "Cemilan",
      tabBarIcon: ({ tintColor }) => (
        <Iconicon name="food-apple-outline" size={20} color={tintColor} />
      )
    }
  },
},
  {
    tabBarOptions: {
      labelStyle: {
        fontSize: 10,
      },
      tabStyle: {
        elevation: 0
      },
      style: {
        backgroundColor: '#fff',
        elevation: 1
      },
      indicatorStyle: {
        backgroundColor: '#2ecc71',
        color: '#2ecc71'
      },
      showIcon: true,
      activeTintColor: '#2ecc71',
      tintColor: '#bdbdbd',
      inactiveTintColor: '#bdbdbd',
    }
  })


const stackResto = createStackNavigator({
  Main: {
    screen: OrderTab
  },
  DetailOrder: {
    screen: DetailOrder
  },
  BillOrder: {
    screen: BillOrder
  },
  Success: {
    screen: Success
  }
  
},
{
  headerMode: 'screen',
  mode: 'modal',
  headerMode: 'none',
}
)




export default stackResto