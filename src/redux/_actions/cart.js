import axios from 'axios'
import { API_URL } from 'react-native-dotenv'
// import AsyncStorage from '@react-native-community/async-storage';

export const tambahPesanan = (data,id) => {
    dataorder = {
        id_transaction: id,
        menu_id: data.id,
        price: data.price,
        status: 1,
        image: data.image,
        name: data.name,
        qty: 1
    }
    return {
        type: 'TAMBAH_PESANAN',
        payload: dataorder
    }
}

export const hapusPesanan = (item, data, datafix) => {

    return {
        type: 'HAPUS_PESANAN',
        payload: item,
        data,
        datafix
    }
}

export const TambahOrders  = (data) => {
    return {
        type: 'POST_ORDERS',
        payload: axios.post(API_URL + 'Orders', data)
       
    }
}


export const resetbill  = () => {
    return {
        type: 'RESET_BILL',
    }
}


export const Increment = (data, datapatch, datafix) => {
    return {
        type: 'INCREMENT',
        payload: data,
        datapatch,
        datafix
    }
}

export const Decrement = (data, datapatch, datafix) => {
    return {
        type: 'DECREMENT',
        payload: data,
        datapatch,
        datafix
    }
}
