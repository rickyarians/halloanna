import axios from 'axios'
import { API_URL } from 'react-native-dotenv'
// import AsyncStorage from '@react-native-community/async-storage';




export const getTransaction  = (id) => {
    return {
        type: 'GET_TRANSACTION',
        payload: axios.get(API_URL + 'Orders/transaction/' + id)
       
    }
}

export const updateOrder = (item, data, datafix) => {
    return {
        type: 'UPDATE_ORDER',
        payload: item,
        data: data,
        datafix: datafix
       
    }
}


export const changeStatusOrder  = (id) => {
    return {
        type: 'UPDATE_TRANSACTION_ORDER',
        payload: axios.patch(API_URL + 'Orders/transaction/' + id)
       
    }
}


export const resetbill  = () => {
    return {
        type: 'RESET_BILL',
    }
}


export const changeStatusTrx  = (id, time, detailtransaction) => {
    const data = {
        finishedTime: time,
        subtotal: detailtransaction.subtotal,
        total: detailtransaction.total,
        discount: detailtransaction.discount,
        serviceCharge: detailtransaction.service,
        tax: detailtransaction.tax,
        isPaid:0,
    }
    return {
        type: 'UPDATE_TRANSACTION_ORDER',
        payload: axios.patch(API_URL + 'transactions/update/' + id, data)
       
    }
}






