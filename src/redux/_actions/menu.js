import axios from 'axios'
import { API_URL } from 'react-native-dotenv'
// import AsyncStorage from '@react-native-community/async-storage';

export const getDataMakanan = () => {
    console.log(API_URL)
    return {
        type: 'GET_MAKANAN',
        payload: axios.get(API_URL + 'categories/menulist/3')
    }
}

export const getDataMinuman = () => {
    console.log(API_URL)
    return {
        type: 'GET_MINUMAN',
        payload: axios.get(API_URL + 'categories/menulist/4')
    }
}

export const getDataCemilan = () => {
    console.log(API_URL)
    return {
        type: 'GET_CEMILAN',
        payload: axios.get(API_URL + 'categories/menulist/5')
    }
}

export const getDataPromo = () => {
    console.log(API_URL)
    return {
        type: 'GET_PROMO',
        payload: axios.get(API_URL + 'categories/menulist/6')
    }
}

export const getDataProduk = () => {
    console.log(API_URL)
    return {
        type: 'GET_PRODUK',
        payload: axios.get(API_URL + 'categories/menulist/')
    }
}


export const UpdateMakanan = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'UPDATE_MAKANAN',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}

export const FalseMakanan = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'FALSE_MAKANAN',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}

export const UpdatePromo = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'UPDATE_PROMO',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}

export const UpdateMinuman = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'UPDATE_MINUMAN',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}

export const UpdateCemilan = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'UPDATE_CEMILAN',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}



export const UpdateMakananFalse = (item, menu, menufix) => {
    console.log(API_URL)
    return {
        type: 'UPDATE_MAKANAN_FALSE',
        payload: item,
        menu: menu,
        menufix:menufix
    }
}

