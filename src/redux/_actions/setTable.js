import axios from 'axios'
import { API_URL } from 'react-native-dotenv'
// import AsyncStorage from '@react-native-community/async-storage';

export const handlingtable = (table) => {
    return {
        type: 'NUMBER_HANDLING',
        payload: table
    }
}

export const mainload = () => {
    return {
        type: 'MAIN_LOAD'
    }
}

export const SetTime = () => {
    return {
        type: 'SET_MULAI'
    }
}

export const SetTimemulai = () => {
    return {
        type: 'SET_MULAI_LAGI'
    }
}

export const changesuccess = () => {
    return {
        type: 'SUCCESS_ORDER'
    }
}

export const changeunsuccess = () => {
    return {
        type: 'UNSUCCESS_ORDER'
    }
}

export const postTable = (number) => {
    data = {
        tableNumber: number
    }
    console.log(API_URL)
    return {
        type: 'POST_TABLE',
        payload: axios.post(API_URL + 'transactions', data)

    }
}

export const setTime = () => {
    return {
        type: 'SET_TIME'
    }
}
