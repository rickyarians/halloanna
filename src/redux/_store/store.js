import {createStore, combineReducers, applyMiddleware} from 'redux'

import cart from './../_reducers/cart'
import menu from './../_reducers/menu'
import timer from './../_reducers/timer'
import setTable from './../_reducers/setTable'
import transaction from './../_reducers/transaction'
import { logger,promise } from './../_middleware/middleware';

// this global states
const reducers = combineReducers({
  setTable,
  menu,
  cart,
  timer,
  transaction
//   auth
})

const store = createStore(
reducers,
  applyMiddleware(logger, promise)
);

export default store