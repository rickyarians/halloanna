

const initialState = {
    Cart: [],
    isLoading: false,
    berhasilbill: false,
    CartFix: []
}

const cart = (state = initialState, action) => {
    switch (action.type) {
        case 'TAMBAH_PESANAN':
            return {
                ...state,
                Cart: [...state.Cart, action.payload]
            }
        case 'HAPUS_PESANAN':


            action.datafix.splice(action.datafix.findIndex(e => e.menu_id === action.payload.menu_id), 1);
            if (action.datafix.length == 0) {
                return {
                    ...state,
                    Cart: []
                }
            } else {
                return {
                    ...state,
                    Cart: action.datafix
                }
            }

        case 'POST_ORDERS':
            return {
                ...state,
                isLoading: true
            }
        case 'POST_ORDERS_FULFILLED':
            return {
                ...state,
                isLoading: false,
                berhasilbill: true,
                CartFix: state.Cart,
                Cart: []
            }
        case 'RESET_BILL':
            return {
                ...state,
                berhasilbill: false,
                Cart: []
            }
        case 'POST_ORDERS_REJECTED':
            return {
                ...state,
                isLoading: false,
                message: action.payload.message
            }
        case 'INCREMENT':
            let dataincrement = action.datapatch.findIndex(y => y.menu_id == action.payload.menu_id)
            let qtynew = action.payload.qty + 1
            let dataincrementfix = { ...action.datapatch[dataincrement], qty: qtynew }
            action.datapatch = action.datapatch.splice(dataincrement, 1, dataincrementfix)
            let dataincrementbanget = [...action.datafix, dataincrementfix]
            dataincrementbanget.pop()
            return {
                ...state,
                Cart: dataincrementbanget
            }
        case 'DECREMENT':
            let datadecrement = action.datapatch.findIndex(y => y.menu_id == action.payload.menu_id)
            let qtydecrement = action.payload.qty - 1
            let datadecrementfix = { ...action.datapatch[datadecrement], qty: qtydecrement }
            action.datapatch = action.datapatch.splice(datadecrement, 1, datadecrementfix)
            let datadecrementbanget = [...action.datafix, datadecrementfix]
            datadecrementbanget.pop()
            return {
                ...state,
                Cart: datadecrementbanget
            }
        default:
            return state;
    }
}


export default cart