

const initialState = {
    isLoading: true,
    order: [],
    transaction: [],
    message: '',
}

const transaction = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_TRANSACTION_ORDER':
            return {
                ...state,
                isLoading: true
            }
        case 'UPDATE_TRANSACTION_ORDER_FULFILLED':
            return {
                ...state,
                isLoading: false
            }
        case 'UPDATE_TRANSACTION_ORDER_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'UPDATE_TRANSACTION_TRX':
            return {
                ...state,
                isLoading: true
            }
        case 'UPDATE_TRANSACTION_TRX_FULFILLED':
            return {
                ...state,
                isLoading: false
            }
        case 'UPDATE_TRANSACTION_TRX_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'GET_TRANSACTION':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_TRANSACTION_FULFILLED':
            return {
                ...state,
                order: action.payload.data.order,
                transaction: action.payload.data.detailTransaction,
                berhasilbill: true,
                isLoading: false
            }
        case 'GET_TRANSACTION_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'UPDATE_ORDER':
            let databill = action.data.findIndex(y => y.menu == action.payload.menu)
            console.log(action.data[databill])

            console.log(action.datafix)
            let databillfix = { ...action.data[databill], status: 1 }

            action.menu = action.data.splice(databill, 1, databillfix)
            let databillfixbanget = [...action.datafix, databillfix]
            // console.log(datamakananfix)
            // console.log(datamakananfixbanget)
            databillfixbanget.pop()
            return {
                ...state,
                order: databillfixbanget
            }
        default:
            return state;
    }
}


export default transaction