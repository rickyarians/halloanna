const initialState = {
    isLoading: false,
    table_number: '',
    idOrder: '',
    message: '',
    berhasilsetTable: false,
    counter: 0,
    successorder: false,
    mulai: false
}

const setTable = (state = initialState, action) => {
    switch (action.type) {
        case 'MAIN_LOAD':
            return {
                ...state,
                berhasilsetTable: false,
            }
        case 'SUCCESS_ORDER':
            return {
                ...state,
                successorder: true,
            }
        case 'UNSUCCESS_ORDER':
            return {
                ...state,
                successorder: false,
                berhasilsetTable: false,
                table_number: '',
                mulai: false,
            }
        case 'SET_MULAI':
            return {
                ...state,
                mulai: true
            }
        case 'SET_MULAI_LAGI':
            return {
                ...state,
                mulai: false
            }
        case 'NUMBER_HANDLING':
            return {
                ...state,
                table_number: action.payload
            }
        case 'POST_TABLE':
            return {
                ...state,
                isLoading: true
            }
        case 'POST_TABLE_FULFILLED':
            return {
                ...state,
                table_number: action.payload.data.tableNumber,
                idOrder: action.payload.data.id,
                isLoading: false,
                berhasilsetTable: true
            }
        case 'POST_TABLE_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        default:
            return state;
    }
}


export default setTable