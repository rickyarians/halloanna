

const initialState = {
    isLoading: true,
    dataProduk: [],
    dataMinuman: [],
    dataCemilan: [],
    dataPromo: []
}

const menu = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_PROMO':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_PROMO_FULFILLED':
            if (action.payload.data[0] != null) {
                const datapromo = action.payload.data[0].menuList.map(item => ({
                    ...item, selected: false
                }))
                return {
                    ...state,
                    dataPromo: datapromo,
                    isLoading: false
                }
            } else {
                return {
                    ...state,
                    dataPromo: action.payload.data,
                    isLoading: false
                }
            }

        case 'GET_PROMO_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'GET_MAKANAN':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_MAKANAN_FULFILLED':
            const data = action.payload.data[0].menuList.map(item => ({
                ...item, selected: false
            }))
            return {
                ...state,
                dataProduk: data,
                isLoading: false
            }
        case 'GET_MAKANAN_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'GET_MINUMAN':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_MINUMAN_FULFILLED':
            const dataminum = action.payload.data[0].menuList.map(item => ({
                ...item, selected: false
            }))
            return {
                ...state,
                dataMinuman: dataminum,
                isLoading: false
            }
        case 'GET_MINUMAN_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'GET_CEMILAN':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_CEMILAN_FULFILLED':
            const datacemilanget = action.payload.data[0].menuList.map(item => ({
                ...item, selected: false
            }))
            return {
                ...state,
                dataCemilan: datacemilanget
            }
        case 'GET_CEMILAN_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'GET_PRODUK':
            return {
                ...state,
                isLoading: true
            }
        case 'GET_PRODUK_FULFILLED':
            return {
                ...state,
                dataProduk: action.payload.data.menuList
            }
        case 'GET_PRODUK_REJECTED':
            return {
                ...state,
                message: action.payload.message,
                isLoading: false
            }
        case 'UPDATE_MAKANAN':
            let datamakanan = action.menu.findIndex(y => y.id == action.payload.id)
            console.log(action.menu[datamakanan])

            console.log(action.menufix)
            let datamakananfix = { ...action.menu[datamakanan], selected: true }

            action.menu = action.menu.splice(datamakanan, 1, datamakananfix)
            let datamakananfixbanget = [...action.menufix, datamakananfix]
            // console.log(datamakananfix)
            // console.log(datamakananfixbanget)
            datamakananfixbanget.pop()
            return {
                ...state,
                dataProduk: datamakananfixbanget
            }
        case 'FALSE_MAKANAN':
            let datamakananfalse = action.menu.findIndex(y => y.id == action.payload.menu_id)
            console.log(action.menu[datamakananfalse])

            console.log(action.menufix)
            let datamakananfalsefix = { ...action.menu[datamakananfalse], selected: false }

            action.menu = action.menu.splice(datamakananfalse, 1, datamakananfalsefix)
            let datamakananfalsefixbanget = [...action.menufix, datamakananfalsefix]
            // console.log(datamakananfix)
            // console.log(datamakananfixbanget)
            datamakananfalsefixbanget.pop()
            return {
                ...state,
                dataProduk: datamakananfalsefixbanget
            }
        case 'UPDATE_MINUMAN':
            let dataminuman = action.menu.findIndex(y => y.id == action.payload.id)
            console.log(action.menu[dataminuman])
            // console.log(datamakanan)
            // console.log(action.menu)
            console.log(action.menufix)
            let dataminumanfix = { ...action.menu[dataminuman], selected: true }

            action.menu = action.menu.splice(dataminuman, 1, dataminumanfix)
            let dataminumanfixbanget = [...action.menufix, dataminumanfix]
            // console.log(datamakananfix)
            // console.log(datamakananfixbanget)
            dataminumanfixbanget.pop()
            return {
                ...state,
                dataMinuman: dataminumanfixbanget
            }
        case 'UPDATE_CEMILAN':
            let datacemilan = action.menu.findIndex(y => y.id == action.payload.id)
            let datacemilanfix = { ...action.menu[datacemilan], selected: true }
            action.menu = action.menu.splice(datacemilan, 1, datacemilanfix)
            let datacemilanfixbanget = [...action.menufix, datacemilanfix]
            datacemilanfixbanget.pop()
            return {
                ...state,
                dataCemilan: datacemilanfixbanget
            }
        case 'UPDATE_PROMO':
            let datapromoin = action.menu.findIndex(y => y.id == action.payload.id)
            console.log(action.menu[datapromoin])
            // console.log(datamakanan)
            // console.log(action.menu)
            console.log(action.menufix)
            let datapromofix = { ...action.menu[datapromoin], selected: true }

            action.menu = action.menu.splice(datapromoin, 1, datapromofix)
            let datapromofixbanget = [...action.menufix, datapromofix]
            // console.log(datamakananfix)
            // console.log(datamakananfixbanget)
            datapromofixbanget.pop()
            return {
                ...state,
                dataPromo: datapromofixbanget
            }
     
        default:
            return state;
    }
}


export default menu